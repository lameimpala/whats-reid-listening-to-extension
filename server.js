require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

app.use(cors());
app.use(express.json());

app.post("/play", async (req, res, next) => {
  try {
    console.log(JSON.stringify(req.body));

    const conn = mongoose.createConnection(process.env.MONGO_CONNECTION_STR);
    const play = await conn
      .useDb(process.env.DB_NAME)
      .collection(process.env.COLLECTION_NAME)
      .insertOne(req.body);

    res.json(play);
  } catch (err) {
    console.error(err.message);
  }
});

app.listen(3001, () => {
  console.log("Listening on 3001");
});
