# Getting Started

You'll need to be using Chrome, and to follow [these steps](https://knowledge.workspace.google.com/kb/load-unpacked-extensions-000005962) to allow installing unpacked extensions.

Once the extension is installed, launch the backend with `npm i && npm start`
