let songThatWasPreviouslyRecorded = null;
let songCurrentlyPlaying = null;

async function checkForNewSong() {
  const imageUrl = document.querySelector('[width="1048"]').getAttribute("src");
  const song = document.querySelector(".title.ytmusic-player-bar").textContent;
  const [artist, album, year] = document
    .querySelector(".byline.ytmusic-player-bar")
    .getAttribute("title")
    ?.split(" • ");

  if (song !== songCurrentlyPlaying && song !== songThatWasPreviouslyRecorded) {
    songCurrentlyPlaying = song;
    setTimeout(async () => {
      if (
        imageUrl &&
        song &&
        artist &&
        album &&
        year &&
        song === songCurrentlyPlaying
      ) {
        await fetch("http://localhost:3001/play", {
          method: "POST",
          headers: new Headers({
            "Content-Type": "application/json",
          }),
          body: JSON.stringify({
            artist,
            album,
            song,
            imageUrl,
            time: new Date().valueOf(),
          }),
        });

        songThatWasPreviouslyRecorded = song;
      }
    }, 20000);
  }
}

setInterval(checkForNewSong, 5000);
